<?php
if($_GET['t']=='mrmiagi'){
    $files = array();
	$currDay = null;
	$filenames = glob("/Users/ibook/Sites/bss/mov/*.mov");
    foreach ($filenames as $filename) {
		$html_string = null;
        $filenamearr = explode('/',$filename);
        $dnamearr = explode('-',$filenamearr[6]);
        $dname = date("d. M Y - H:i:s",mktime($dnamearr[3], $dnamearr[4], $dnamearr[5], $dnamearr[1], $dnamearr[2], $dnamearr[0]));
        $html_string .= '<li class="arrow slide">';
        $html_string .= '<a href="#video" id="'.$filenamearr[6].'">'.$dname.'<br>'.format_bytes(filesize($filename)).'</a>';
        $html_string .= '</li>';
        $files[] = $html_string;
    }
    $files = array_reverse($files);
	$files = array_slice($files, 0, 10);
} else {
    header('HTTP/1.1 403 Forbidden');
    die();
}

function format_bytes($size) {
    $units = array(' B', ' KB', ' MB', ' GB', ' TB');
    for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
    return round($size, 2).$units[$i];
}
?>
<!doctype html>
<html lang="en">
<head>
	<title>CTRL Center</title>
    <meta charset="UTF-8" />
    <style media="screen">@import "jqtouch/jqtouch.css";</style>
    <style media="screen">@import "themes/jqt/theme.css";</style>
	<style type="text/css" media="screen">@import "extensions/jqt.bars/jqt.bars.css";</style>
	<style type="text/css" media="screen">@import "extensions/jqt.bars/themes/jqt/theme.css";</style>


    <script src="jqtouch/jquery-1.4.2.min.js"></script>
    <script src="jqtouch/jqtouch.js"></script>
	<script src="extensions/jqt.bars/jqt.bars.js"></script>
	<script type="text/javascript" charset="utf-8">
        var jqt = $.jQTouch({
			statusBar: 'black',
            icon: 'icon.png',
            startupScreen: 'startup.png'
        });

		function stat(){
			$('#stat').html('<img src="images/loading.gif" style="margin-top:4px;">');
			$.getJSON("status?t=<?php echo $_GET['t'];?>", function(data){
				if(data){
					$('#stat').html('armed').attr('href', '#stop');
				} else {
					$('#stat').html('disarmed').attr('href', '#start');
				}
			});
		}
		
		$(function(){

/*			$('#stat').bind('tap', function(e){
				$('#stat').html('<img src="images/loading.gif" style="margin-top:4px;">');
				e.preventDefault();
				if( $('#stat').html() == 'armed'){		
					$.get("toggleEvoCam.php?t=<?php echo $_GET['t'];?>&a=stop", function(){
						stat();
					});
				} else {
					$.get("toggleEvoCam.php?t=<?php echo $_GET['t'];?>&a=start", function(){
						stat();
					});
				}
				return false;
			});
*/			
			$('#start').bind('pageAnimationStart', function(e, info){
				if(info.direction == 'in'){
					$.get("toggleEvoCam.php?t=<?php echo $_GET['t'];?>&a=start");
				} else {
					stat();
				}
           	});

			$('#stop').bind('pageAnimationStart', function(e, info){
				if(info.direction == 'in'){
					$.get("toggleEvoCam.php?t=<?php echo $_GET['t'];?>&a=stop");
				} else {
					stat();
				}
           	});

			$('#videolist a').bind('tap', function(){
				$('#video video').attr('src', 'mov/' + $(this).attr('id'));
			});
			
			$('#live').bind('pageAnimationStart', function(e, info){
				if(info.direction == 'in' && $('#stat').html() == 'armed'){
					$('#live iframe').attr('src', 'http://bss:mrmiagi@home.brainfusion.at:8080/webcam.html');
				} else {
					$('#live iframe').attr('src', '');
				}
			});
			
			window.setTimeout(function() {
			    stat();
			}, 300);
			
		});
	</script>
</head>
<body>
	<div id="jqt">
		<div id="home" class="s-pane">
			<div class="toolbar">
                <h1>BSS</h1>
				<a href="" id="stat" class="button leftButton"></a>
		       	<a href="#live" class="button rightButton">Live video</a>
            </div>
		    <div id="home-wrapper" class="s-scrollwrapper">
				<div id="home-pane" class="s-scrollpane">
					<ul id="videolist" class="rounded">
						<?php
				   		foreach($files as $file){
			            	echo $file;
				   		};
						?>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="start" class="s-pane">
			<div class="toolbar">
                <h1>BSS</h1>
		       	<a href="#" class="back">Home</a>
		       	<a href="http://home.brainfusion.at:8080/webcam.html" class="button rightButton">Live video</a>
            </div>
		    <div id="start-wrapper" class="s-scrollwrapper">
				<div id="start-pane" class="s-scrollpane">BSS<br />arming…</div>
			</div>
		</div>
		
		<div id="stop" class="s-pane">
			<div class="toolbar">
                <h1>BSS</h1>
		       	<a href="#" class="back">Home</a>
		       	<a href="http://home.brainfusion.at:8080/webcam.html" class="button rightButton">Live video</a>
            </div>
		    <div id="stop-wrapper" class="s-scrollwrapper">
				<div id="stop-pane" class="s-scrollpane">BSS<br />disarming…</div>
			</div>
		</div>
		
		<div id="video" class="s-pane">
			<div class="toolbar">
				<h1>Video</h1>
				<a href="#" class="back">Home</a>
		       	<a href="http://home.brainfusion.at:8080/webcam.html" class="button rightButton">Live video</a>
           	</div>
			<div id="video-wrapper" class="s-scrollwrapper">
				<div id="video-pane" class="s-scrollpane">
          			<video id="movie" src="" width="320" height="415"></video>
				</div>
			</div>
        </div>

		<div id="live" class="s-pane">
			<div class="toolbar">
				<h1>Live Video</h1>
				<a href="#" class="back">Home</a>
			</div>
			<div id="live-wrapper" class="s-scrollwrapper">
				<div id="live-pane" class="s-scrollpane">
					<iframe frameborder="0" marginheight="0" marginwidth="0" width="100%" height="100%" scrolling="no" src="" />
				</div>
			</div>
		</div>
	    
	</div>
</body>
</html>